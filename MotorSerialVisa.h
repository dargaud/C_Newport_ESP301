#ifndef _CCOB_MOTOR_SERIAL_H
#define _CCOB_MOTOR_SERIAL_H

#include <visa.h>

extern int    MotorInit       (ViSession Resource, const ViRsrc Descr, ViSession *Instr, int Home,
							   double *Max1, double *Max2, char* Unit1, char* Unit2, char* Msg);
extern void   MotorStop       (ViSession Instr, int NoSend);

extern int    MotorOn         (ViSession Instr, int Motor, int On);
extern int    MotorGoRef      (ViSession Instr, int Motor);
extern int    MotorDefRef     (ViSession Instr, int Motor);
extern int    MotorMove       (ViSession Instr, int Motor, double Pos);
extern int    MotorStatus     (ViSession Instr, int Motor);
extern void   MotorAllStopped (ViSession Instr);
extern double MotorGetPos     (ViSession Instr, int Motor);
extern char*  MotorGetIdentity(ViSession Instr, char* Unit1, char* Unit2);
extern void   MotorFindLimits (ViSession Instr, double *Max1, double *Max2);

extern char*  MotorSetSpeeds  (ViSession Instr);
extern char*  MotorGetAccel   (ViSession Instr);
extern char*  MotorUnits      (ViSession Instr, char* Unit1, char* Unit2);
extern int    MotorControllerStatus  (ViSession Instr, char* StatusStr);
extern int    MotorControllerActivity(ViSession Instr, char* StatusStr);
extern unsigned long long
			  MotorHardwareStatus    (ViSession Instr, char* StatusStr);
extern int    MotorErrorMessage      (ViSession Instr, char* ErrMsg);

#endif