#include "Diammoni.h"

///////////////////////////////////////////////////////////////////////////////
// VISA driver for the ESP301 motor controller
// It should work in either USB or serial Mode, the speed is autodetected
// Author: Guillaume Dargaud 2022 (2018 for original Linux version)
///////////////////////////////////////////////////////////////////////////////


#include <ansi_c.h>
#include <iso646.h>

#include <cvirte.h>
#include <utility.h>
#include <userint.h>	// For the popup
#include <visa.h>

#include "MotorSerialVisa.h"

#undef DD
#define DD 0.5//0.1	// Delay for polling, in s

#define TERM "\r"	// "\r\n". The \n is implied by VI_ATTR_TERMCHAR_EN
//#define TERM ""	// "\r". The \r is implied by VI_ATTR_TERMCHAR_EN

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Turn a motor on or off
/// HIPAR	Motor / 1 or 2
/// HIPAR	On / 0 for off, 1 for on
/// HIRET	0 off, 1 on, <0 Possible error code
///////////////////////////////////////////////////////////////////////////////
int MotorOn(ViSession Instr, int Motor, int On) {
	if (!Instr) return 0;
	int R=viPrintf(Instr, "%dM%c"TERM, Motor, On ? 'O' : 'F');
	if (R) return R;
	int Rep=2;
	R=viQueryf(Instr, "%dMO?"TERM, "%d", Motor, &Rep);
	if (R and Rep==2) Rep=R;
	return Rep;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Go to negative hardware travel limit
/// HIPAR	Motor / 1 or 2
/// HIRET	Possible VISA error code
///////////////////////////////////////////////////////////////////////////////
int MotorGoRef(ViSession Instr, int Motor) {
	if (!Instr) return 0;	// TODO: compare with OR (Origin search)
	return viPrintf(Instr, "%dMT-"TERM, Motor);	// Move to hardware travel limit, negative direction
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Define axis reference
/// HIPAR	Motor / 1 or 2
/// HIRET	Possible VISA error code
///////////////////////////////////////////////////////////////////////////////
int MotorDefRef(ViSession Instr, int Motor) {
	if (!Instr) return 0;
	return viPrintf(Instr, "%dDH"TERM, Motor);	// Define home
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Define reference positions and find max values
/// OUT		Max1, Max2
/// HIPAR	Max1/Returned max position for axis 1
/// HIPAR	Max2/Returned max position for axis 2
///////////////////////////////////////////////////////////////////////////////
void MotorFindLimits(ViSession Instr, double *Max1, double *Max2) {
	if (!Instr) return;
	double PosX=*Max1=MotorGetPos(Instr, 1);
	double PosY=*Max2=MotorGetPos(Instr, 2);
//	printf("Starting motor pos: % 7.3f % 7.3f\n", *Max1, *Max2);

//	viPrintf(Instr, "1MT-"TERM);	// Move to hardware travel limit, negative direction
//	viPrintf(Instr, "2MT-"TERM);
	viPrintf(Instr, "1OR4"TERM);	// Homing, negative direction
	viPrintf(Instr, "2OR4"TERM);
	MotorAllStopped(Instr);

	viPrintf(Instr, "1DH"TERM);		// Define home. May not be necessary when using 1OR4
	viPrintf(Instr, "2DH"TERM);

	viPrintf(Instr, "1MT+"TERM);	// Move to hardware travel limit, positive direction
	viPrintf(Instr, "2MT+"TERM);
//	viPrintf(Instr, "1OR3"TERM);	// Homing, positive direction. No, this redefines the home
//	viPrintf(Instr, "2OR3"TERM);
	MotorAllStopped(Instr);

	*Max1=MotorGetPos(Instr, 1);
	*Max2=MotorGetPos(Instr, 2);
//	printf("Positive pos: % 7.3f % 7.3f\n", *Max1, *Max2);

	// Back to original position (
	MotorMove(Instr, 1, PosX);	//(int)*Max1/2/10*10);	// Go to the middle (rounded, should be 50)
	MotorMove(Instr, 2, PosY);	//(int)*Max2/2/10*10);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Find current speed limits and sets safe ones
/// HIRET	Explanatory string
///////////////////////////////////////////////////////////////////////////////
char* MotorSetSpeeds(ViSession Instr) {
	if (!Instr) return 0;
	double VU1, VU2, OH1, OH2, OL1, OL2, VA1, VA2;
	static char Str[1024]="";
	viQueryf(Instr, "1VU?"TERM, "%lf", &VU1);	// Max velocity
	viQueryf(Instr, "2VU?"TERM, "%lf", &VU2);
	viQueryf(Instr, "1OH?"TERM, "%lf", &OH1);	// Home search high speed
	viQueryf(Instr, "2OH?"TERM, "%lf", &OH2);
	viQueryf(Instr, "1OL?"TERM, "%lf", &OL1);	// Home search high speed
	viQueryf(Instr, "2OL?"TERM, "%lf", &OL2);
	viQueryf(Instr, "1VA?"TERM, "%lf", &VA1);	// Current velocity
	viQueryf(Instr, "2VA?"TERM, "%lf", &VA2);
	sprintf(Str, "Max velocities:         % 7.3f, % 7.3f\n"
				 "Home search high speed: % 7.3f, % 7.3f\n"
				 "Home search low speed:  % 7.3f, % 7.3f\n"
				 "Current speeds:         % 7.3f, % 7.3f",
				 VU1, VU2, OH1, OH2, OH1, OH2, VA1, VA2);

	viPrintf(Instr, "1OH15"TERM);				// Home search high speed
	viPrintf(Instr, "2OH15"TERM);
	viPrintf(Instr, "1OL5"TERM);				// Home search low speed
	viPrintf(Instr, "2OL5"TERM);
	viPrintf(Instr, "1VA50"TERM);				// Normal velocity
	viPrintf(Instr, "2VA30"TERM);				// Slower because more mass

	return Str;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Find acceleration limits AC/AU/AF
/// HIRET	Explanatory string
///////////////////////////////////////////////////////////////////////////////
char* MotorGetAccel(ViSession Instr) {
	if (!Instr) return 0;
	double AU1, AU2, AC1, AC2, AF1, AF2;
	static char Str[1024]="";
	viQueryf(Instr, "1AU?"TERM, "%lf", &AU1);	// Maximum acceleration
	viQueryf(Instr, "2AU?"TERM, "%lf", &AU2);
	viQueryf(Instr, "1AC?"TERM, "%lf", &AC1);	// Current acceleration
	viQueryf(Instr, "2AC?"TERM, "%lf", &AC2);
	viQueryf(Instr, "1AF?"TERM, "%lf", &AF1);	// Acceleration/Deceleration feed-forward gain
	viQueryf(Instr, "2AF?"TERM, "%lf", &AF2);
	sprintf(Str, "Max acceleration:       % 7.3f, % 7.3f\n"
				 "Accel feedforward gain: % 7.3f, % 7.3f\n"
				 "Current acceleration:   % 7.3f, % 7.3f",
				 AU1, AU2, AF1, AF2, AC1, AC2);

	viPrintf(Instr, "1AC100"TERM);				// Normal Accel
	viPrintf(Instr, "2AC50"TERM);				// Slower because more mass

	return Str;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Deal with units
/// OUT		Unit1, Unit2
/// HIPAR	Unit1/If non NULL, will contain text of unit on axis 1
/// HIPAR	Unit2/If non NULL, will contain text of unit on axis 2
/// HIRET	Explanatory string
///////////////////////////////////////////////////////////////////////////////
char* MotorUnits(ViSession Instr, char* Unit1, char* Unit2) {
	if (!Instr) return 0;
	char *SN2S[]={ "encoder count", "motor step", "millimeter", "micrometer",
				  "inches", "milli-inches", "micro-inches",
				  "degree", "gradian", "radian", "milliradian", "microradian"};
	int SN1, SN2;
	double FR1, FR2, SU1, SU2;
	static char Str[1024]="";
	viQueryf(Instr, "1SN?"TERM, "%d",  &SN1);	// Unit
	viQueryf(Instr, "2SN?"TERM, "%d",  &SN2);
	viQueryf(Instr, "1FR?"TERM, "%lf", &FR1);	// Full step resolution
	viQueryf(Instr, "2FR?"TERM, "%lf", &FR2);
	viQueryf(Instr, "1SU?"TERM, "%lf", &SU1);	// Encoder resolution
	viQueryf(Instr, "2SU?"TERM, "%lf", &SU2);
	sprintf(Str, "Units:                 %s, %s\n"
				 "Full step resolutions: %.4f, %.4f\n"
				 "Encoder resolutions:   %.4f, %.4f",
		   SN2S[SN1], SN2S[SN2], FR1, FR2, SU1, SU2);
	if (Unit1) strcpy(Unit1, SN2S[SN1]);
	if (Unit2) strcpy(Unit2, SN2S[SN2]);
	return Str;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Motor move. Returns instantly.
/// HIPAR	Motor / 1 or 2
/// HIPAR	Pos / Position in mm
///////////////////////////////////////////////////////////////////////////////
int MotorMove(ViSession Instr, int Motor, double Pos) {
	if (!Instr) return 0;
	return viPrintf(Instr, "%dPA%.3f"TERM, Motor, Pos);	// Move to absolute position
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get status
/// HIPAR	Motor / 1 or 2
/// HIRET	TRUE if currently moving, 0 if stopped or no reply - Warning, MD returns 1 when stopped
///////////////////////////////////////////////////////////////////////////////
int MotorStatus(ViSession Instr, int Motor) {
	if (!Instr) return 0;
	int Rep=2;	// Assumed to be stopped by default
	viQueryf(Instr, "%dMD?"TERM, "%d", Motor, &Rep);	// Read motion done status
	return Rep==0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get controller status (for 3 axes)
/// HIPAR	StatusStr / String where to write status message (or NULL)
/// HIRET	16-bit status code (see TS documentation)
///////////////////////////////////////////////////////////////////////////////
int MotorControllerStatus(ViSession Instr, char* StatusStr) {
	if (!Instr) return 0;
	char L=0, R=0;
	viQueryf(Instr, "TS"TERM, "%c%c", &L, &R);	// Read motion done status
	if (StatusStr) sprintf(StatusStr,
							  "Axis #1 motor state: %s\n"
							  "Axis #2 motor state: %s\n"
							  "Axis #3 motor state: %s\n"
							//"Axis #4 motor state: %s\n"
							//"Axis #5 motor state: %s\n"
							//"Axis #6 motor state: %s\n"
							  "Motor power of at least one axis L: %s\n",
							//"Motor power of at least one axis R: %s\n",
							  L&(1<<0) ? "In motion" : "Stationary",
							  L&(1<<1) ? "In motion" : "Stationary",
							  L&(1<<2) ? "In motion" : "Stationary",
							//L&(1<<3) ? "In motion" : "Stationary",
							//R&(1<<0) ? "In motion" : "Stationary",
							//R&(1<<1) ? "In motion" : "Stationary",
							  L&(1<<6) ? "ON" : "OFF"
							//R&(1<<6) ? "ON" : "OFF"
	);
	return (L<<8) | R;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get controller activity
/// HIPAR	StatusStr / String where to write status message (or NULL)
/// HIRET	8-bit status code (see TX documentation)
///////////////////////////////////////////////////////////////////////////////
int MotorControllerActivity(ViSession Instr, char* StatusStr) {
	if (!Instr) return 0;
	char L=0;
	viQueryf(Instr, "TX"TERM, "%c", &L);	// Read motion done status
	if (StatusStr) sprintf(StatusStr,
							  "At least one program is executing:    %s\n"
							  "Wait command is executing:            %s\n"
							  "Manual jog mode is active:            %s\n"
							  "Local mode is inactive:               %s\n"
							  "At least one trajectory is executing: %s\n",
							  L&(1<<0) ? "YES" : "NO",
							  L&(1<<1) ? "YES" : "NO",
							  L&(1<<2) ? "YES" : "NO",
							  L&(1<<3) ? "Default" : "-",
							  L&(1<<4) ? "YES" : "NO"
							 );
	return L;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get hardware status
/// HIPAR	StatusStr / String where to write status message (or NULL)
/// HIRET	64-bit status code (see PH documentation)
///////////////////////////////////////////////////////////////////////////////
unsigned long long MotorHardwareStatus(ViSession Instr, char* StatusStr) {
	if (!Instr) return 0;
	unsigned long L=0, H=0;
	viQueryf(Instr, "PH?"TERM, "%XH, %XH", &L, &H);
	if (StatusStr) sprintf(StatusStr,
							  "Hardware travel limits + axes 1..3: %s %s %s\n"// %s %s %s\n"
							  "Hardware travel limits + axes 1..3: %s %s %s\n"// %s %s %s\n"
							  "Axis 1..3 amplifier fault input:    %s %s %s\n"// %s %s %s\n"
							  "100-pin emergency stop (unlatched):              %s\n"
							  "Auxiliary I/O emergency stop (unlatched):        %s\n"
							  "100-pin connector emergency stop (latched):      %s\n"
							  "auxiliary I/O conector emergency stop (latched): %s\n"
							  "100-pin cable interlock:                         %s\n"
							  "Axis 1..3 home  signal: %s %s %s\n"// %s %s %s\n"
							  "Axis 1..3 index signal: %s %s %s\n"// %s %s %s\n"
							  "Digital inputs A/B/C:   %s %s %s\n",

							  L&(1<< 0) ? "H" : "L",
							  L&(1<< 1) ? "H" : "L",
							  L&(1<< 2) ? "H" : "L",
							//L&(1<< 3) ? "H" : "L",
							//L&(1<< 4) ? "H" : "L",
							//L&(1<< 5) ? "H" : "L",

							  L&(1<< 8) ? "H" : "L",
							  L&(1<< 9) ? "H" : "L",
							  L&(1<<10) ? "H" : "L",
							//L&(1<<11) ? "H" : "L",
							//L&(1<<12) ? "H" : "L",
							//L&(1<<13) ? "H" : "L",

							  L&(1<<16) ? "H" : "L",
							  L&(1<<17) ? "H" : "L",
							  L&(1<<18) ? "H" : "L",
							//L&(1<<19) ? "H" : "L",
							//L&(1<<20) ? "H" : "L",
							//L&(1<<21) ? "H" : "L",

							  L&(1<<27) ? "H" : "L",
							  L&(1<<28) ? "H" : "L",
							  L&(1<<29) ? "H" : "L",
							  L&(1<<30) ? "H" : "L",
							  L&(1<<31) ? "H" : "L",

							  H&(1<< 0) ? "H" : "L",
							  H&(1<< 1) ? "H" : "L",
							  H&(1<< 2) ? "H" : "L",
							//H&(1<< 3) ? "H" : "L",
							//H&(1<< 4) ? "H" : "L",
							//H&(1<< 5) ? "H" : "L",

							  H&(1<< 8) ? "H" : "L",
							  H&(1<< 9) ? "H" : "L",
							  H&(1<<10) ? "H" : "L",
							//H&(1<<11) ? "H" : "L",
							//H&(1<<12) ? "H" : "L",
							//H&(1<<13) ? "H" : "L",

							  H&(1<<16) ? "H" : "L",
							  H&(1<<17) ? "H" : "L",
							  H&(1<<18) ? "H" : "L"
							 );
	return ((unsigned long long)L<<32) | H;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read error message
/// HIPAR	ErrMsg / Last error message ("NO ERROR DETECTED" by default)
/// HIRET	Last Newport Error code. 0 if no error. See TB documentation
///////////////////////////////////////////////////////////////////////////////
int MotorErrorMessage(ViSession Instr, char* ErrMsg) {
	if (!Instr) return 0;
	int Err=-1, Date=0; *ErrMsg=0;
	int R=viQueryf(Instr, "TB?"TERM, "%d, %d, %[^\r]", &Err, &Date, ErrMsg);	// Read motion done status
	if (*ErrMsg==0)	// Probable timeout
		// Doesn't have much sense to Err=R, but I don't want to return 0
		viStatusDesc (Instr, Err=R, ErrMsg);
	return Err;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN    Wait for both motors to stop moving
///////////////////////////////////////////////////////////////////////////////
void MotorAllStopped(ViSession Instr) {
	if (!Instr) return;
	while (MotorStatus(Instr, 2)) Delay(DD);
	while (MotorStatus(Instr, 1)) Delay(DD);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get position
/// HIPAR	Motor / 1 or 2
/// HIRET	position or -1 if unavailable
///////////////////////////////////////////////////////////////////////////////
double MotorGetPos(ViSession Instr, int Motor) {
	if (!Instr) return 0;
	double Rep=-1;
	viQueryf(Instr, "%dTP"TERM, "%lf", Motor, &Rep);	// Read actual position (no '?')
	return Rep;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get identity
/// OUT		Unit1, Unit2
/// HIPAR	Unit1/Unit used by axis 1
/// HIPAR	Unit2/Unit used by axis 2
/// HIRET	Identity string and plenty of other info
///////////////////////////////////////////////////////////////////////////////
char* MotorGetIdentity(ViSession Instr, char* Unit1, char* Unit2) {
	if (!Instr) return 0;
	char Str[1024];
	static char Id[4096]; *Id=0;
	char Res[256];
	int BOLE = SetBreakOnLibraryErrors (0);
	viQueryf(Instr, "1ID?"TERM, "%[^\r]", Res);		// Identity
	// This is the 1st command sent to the device and may fail if the buffer contains garbage
	// so do it again just in case. If it fails again, we have a problem:

	if (viQueryf(Instr, "1ID?"TERM, "%[^\r]", Res) and
		ConfirmPopup ("Newport controller error",
					  "The motor controller is not replying\n"
					 "Would you like to attempt a reset command ?\n"
					 "[There will be a 20s wait]")) {
		viPrintf(Instr, "RS"TERM);					// Reset - Do this only for deep problems - see doc
		Delay(20);
	}
	SetBreakOnLibraryErrors (BOLE);

	viQueryf(Instr, "1ID?"TERM, "%[^\r]", Res);
	sprintf(Id+strlen(Id), "Identity #1: %s\n", Res);
	viQueryf(Instr, "2ID?"TERM, "%[^\r]", Res);
	sprintf(Id+strlen(Id), "Identity #2: %s\n", Res);
	viQueryf(Instr, "VE?"TERM, "%[^\r]", Res);		// Firmware version
	sprintf(Id+strlen(Id), "Firmware version: %s\n", Res);
	viQueryf(Instr, "PH?"TERM, "%[^\r]", Res);		// Hardware status
	sprintf(Id+strlen(Id), "Hardware status:\n%s\n", Res);
	MotorControllerStatus(Instr, Str);
	sprintf(Id+strlen(Id), "Controller status:\n%s\n",   Str);
	MotorControllerActivity(Instr, Str);
	sprintf(Id+strlen(Id), "Controller activity:\n%s\n", Str);
	MotorHardwareStatus(Instr, Str);
	sprintf(Id+strlen(Id), "Hardware status: %s\n", Str);
	sprintf(Id+strlen(Id), "%s\n", MotorUnits(Instr, Unit1, Unit2));
	sprintf(Id+strlen(Id), "\nPurging error message stack:\n");
	while (MotorErrorMessage(Instr, Res))
		sprintf(Id+strlen(Id), "%s\n", Res);
	return Id;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Init sequence for motors. The communication speed is auto-detected (serial or USB)
/// OUT		Instr, Max1, Max2, Msg
/// HIPAR	Home/If 0, do not perform homing calibration (will assume limit is 100)
/// HIPAR	Max1/If either is NULL, will not send any init command to the controller
/// HIPAR	Max2/Otherwise perform zeroing and max detection of axes, returning the max positions
/// HIPAR	Msg/Optional init and info messages (if !=NULL)
/// HIRET   0 for success
///////////////////////////////////////////////////////////////////////////////
int MotorInit(ViSession Resource, const ViRsrc Descr, ViSession *Instr, int Home,
			  double *Max1, double *Max2, char* Unit1, char* Unit2, char* Msg) {
	errno=0;
	if (*Instr!=0) return 1;	// Already open instrument

	int SBOLE=SetBreakOnLibraryErrors(0);
	ViStatus St=viOpen (Resource, Descr, VI_NULL, 5, Instr);
	SetBreakOnLibraryErrors(SBOLE);
	if (*Instr==0 or St) {
		char SD[256];
		viStatusDesc (Resource, St, SD);
		if (Msg) sprintf(Msg, "viOpen %s failed: %s\n", Descr, SD);
		else      printf(     "viOpen %s failed: %s\n", Descr, SD);
		return St;
	}

	ViUInt32 Val;
	viGetAttribute(*Instr, VI_ATTR_TMO_VALUE, &Val);
	if (Msg) sprintf(Msg+strlen(Msg), "Timeout: %dms\n", Val);
//	viSetAttribute(*Instr, VI_ATTR_TMO_VALUE, 2000);	// Seems to already be default value

	// It is only needed for serial communication
	unsigned short Attr=0;
	viGetAttribute (*Instr, VI_ATTR_INTF_TYPE, &Attr);
	if (Attr==VI_INTF_ASRL) {
//		viSetBuf       (*Instr, VI_READ_BUF|VI_WRITE_BUF, 4000);
		char Desc[256]="";
		// 921600 if using USB via serial virtual port provided by usbuart3410.inf driver
		viGetAttribute (*Instr, VI_ATTR_INTF_INST_NAME, Desc);
		int Bauds=(strstr(Desc, "Newport ESP301") ? 921600 : 19200);
		viSetAttribute (*Instr, VI_ATTR_ASRL_BAUD,        Bauds);
		viSetAttribute (*Instr, VI_ATTR_ASRL_DATA_BITS,   8);
		viSetAttribute (*Instr, VI_ATTR_ASRL_PARITY,      VI_ASRL_PAR_NONE);
		viSetAttribute (*Instr, VI_ATTR_ASRL_STOP_BITS,   VI_ASRL_STOP_ONE);
		viSetAttribute (*Instr, VI_ATTR_ASRL_FLOW_CNTRL,  VI_ASRL_FLOW_NONE); //or VI_ASRL_FLOW_DTR_DSR, doesn't matter
		viSetAttribute (*Instr, VI_ATTR_ASRL_FLOW_CNTRL,  VI_ASRL_FLOW_NONE); // It shouldn't be important but in 2021 I HAD to add it
		viSetAttribute (*Instr, VI_ATTR_WR_BUF_OPER_MODE, VI_FLUSH_ON_ACCESS);
		viSetAttribute (*Instr, VI_ATTR_RD_BUF_OPER_MODE, VI_FLUSH_ON_ACCESS);
		viSetAttribute (*Instr, VI_ATTR_TERMCHAR,         '\n');
		viSetAttribute (*Instr, VI_ATTR_TERMCHAR_EN,      VI_FALSE); // WARNING: do not send \n if using this
		viSetAttribute (*Instr, VI_ATTR_ASRL_END_IN,      VI_ASRL_END_TERMCHAR);
		viSetAttribute (*Instr, VI_ATTR_ASRL_END_OUT,     VI_ASRL_END_NONE);
		//viSetAttribute (*Instr, VI_ATTR_SUPPRESS_END_EN,  VI_TRUE);
	}

	viFlush (*Instr, VI_WRITE_BUF);
	viFlush (*Instr, VI_READ_BUF);

	if (Max1==NULL or Max2==NULL) return 0;

	if (Msg) sprintf(Msg+strlen(Msg), "Motor controller:\n%s\n", MotorGetIdentity(*Instr, Unit1, Unit2));
	if (Msg) sprintf(Msg+strlen(Msg), "%s\n",                    MotorSetSpeeds  (*Instr));
	if (Msg) sprintf(Msg+strlen(Msg), "%s\n",                    MotorGetAccel   (*Instr));
	MotorOn(*Instr, 1, 1);
	MotorOn(*Instr, 2, 1);
	if (Home) MotorFindLimits(*Instr, Max1, Max2);
	else *Max1=*Max2=100;
	if (Msg) sprintf(Msg+strlen(Msg), "Positive pos:           % 7.3f % 7.3f\n", *Max1, *Max2);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Stop and close motors
/// HIPAR	NoSend/If 1, will not stop the motors
///////////////////////////////////////////////////////////////////////////////
void MotorStop(ViSession Instr, int NoSend) {
	if (!Instr) return;
	if (!NoSend) {
//		MotorMove(Instr, 1, 0);		// Back to base
//		MotorMove(Instr, 2, 0);
//		MotorAllStopped(Instr);
		MotorOn(Instr, 1, 0);		// Stop
		MotorOn(Instr, 2, 0);
	}
	viClose(Instr); Instr=0;
}


///////////////////////////////////////////////////////////////////////////////
#ifdef TEST_MOTOR
// Compile with: gcc -DTEST_MOTOR -I../Source_ThinBeam CcobMotorSerial.c SerialOpen.c -o TestMotor
#include <math.h>
#define DEV "ASRL3::INSTR"	// or COM4

int main(int argc, char *argv[]) {
	int NOK, lenX=0, lenY=0;
	double MaxX=0, MaxY=0, PosX=0, PosY=0;
	char Msg[10000]="";
	ViSession Resource=0, Instr=0;
//	dup2(1, 2);  // Redirect stderr to stdout below this line.

	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */

	viOpenDefaultRM (&Resource);

	if (argc==2 and 0==strcmp(argv[1], "-h")) {
		fprintf(stderr, "%s\tDo a motion loop\n"
					"%s PosX PosY\tGo to position\n"
					"%s cmd1 [cmd2...]\tSend arbitrary commands (no init motion)\n",
					argv[0], argv[0], argv[0]);
		return 1;
	}

	if (argc==3 and 1==sscanf(argv[1], "%f%n", &PosX, &lenX) and !argv[1][lenX]
				and 1==sscanf(argv[2], "%f%n", &PosY, &lenY) and !argv[2][lenY]) {
		// Go to position
		NOK=MotorInit(Resource, DEV, &Instr, 1, &MaxX, &MaxY, NULL, NULL, Msg);
		puts(Msg);
		if (NOK) return NOK;
		printf("Max pos  : % 7.3f % 7.3f\n", MaxX, MaxY);
		printf("Positions: % 7.3f % 7.3f\n", MotorGetPos(Instr, 1), MotorGetPos(Instr, 2));

		MotorMove(Instr, 1, PosX);
		MotorMove(Instr, 2, PosY);

		MotorAllStopped(Instr);
		printf("Positions: % 7.3f % 7.3f\n", MotorGetPos(Instr, 1), MotorGetPos(Instr, 2));
		MotorOn(Instr, 1, 0);
		MotorOn(Instr, 2, 0);

		MotorStop(Instr, 0);
		goto End;
	}

	if (argc==1) {	// Loop
		NOK=MotorInit(Resource, DEV, &Instr, 1, &MaxX, &MaxY, NULL, NULL, Msg);
		puts(Msg);
		if (NOK) return NOK;
		printf("Max pos  : % 7.3f % 7.3f\n", MaxX, MaxY);
		printf("Positions: % 7.3f % 7.3f\n", MotorGetPos(Instr, 1), MotorGetPos(Instr, 2));

		for (int j=0; j<=2; j++) {
			double y=j*MaxY/2;
			MotorMove(Instr, 2, y);
			for (int i=0; i<=2; i++) {
				double x=i*MaxX/2;
				MotorMove(Instr, 1, x);
				MotorAllStopped(Instr);
				double	XP=MotorGetPos(Instr, 1),
						YP=MotorGetPos(Instr, 2);
				printf("Positions: % 7.3f/% 7.3f % 7.3f/% 7.3f%s\n",
						x, XP, y, YP,
						fabs(x-XP)>1e-2 or fabs(y-YP)>1e-2 ? " *** MisPos ***" : ""
					);
			}
		}
		MotorOn(Instr, 1, 0);
		MotorOn(Instr, 2, 0);

		MotorStop(Instr, 0);
		goto End;
	}

	// Passing straight commands
	char Rep[256]="";
	NOK=MotorInit(Resource, DEV, &Instr, 0, NULL, NULL, NULL, NULL, Msg);
	puts(Msg);
	if (NOK) return NOK;
	for (int i=1; i<argc; i++) {
		viQueryf (Instr, "%s", "%[^\r]", argv[i], Rep);
		printf("Q: %s\nR: %s\n", argv[i], Rep);
	}
	MotorStop(Instr, 1);

End:
	viClose (Resource);
	return 0;
}
#endif
