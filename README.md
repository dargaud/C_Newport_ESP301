# C_Newport_ESP301

This is a Visa driver for the *Newport ESP301* motor controller

## Installation

- works both on normal serial port (possibly via a USB-to-Serial adapter) and the direct USB connection (see below)
- If you want to use the USB connection: unzip the file `ESP301-GUI-V2.0.0.3.zip`  (available from [ESP301_Installer_Win10  here](https://www.newport.com/p/ESP301-3N)) and run the installer within. Then  navigate to `C:\Newport\Motion Control\ESP301\Bin\USB Driver\64-bit` , right-click `usbuart3410.inf` and click install. Reboot. This will install a virtual serial port for the USB connection. Yes, it works on Win10.

## Motors configuration

The Newport has 2 connection modes:
- a serial port at 19200 8N1. It shows in NI-MAX as `ASRL5::INSTR` (COM5) or similar
- a USB port, via a virtual serial driver at 921600 8N1 which also shows in NI-MAX as `ASRL3::INSTR` (COM3) or similar. If the driver is properly installed, in the Windows Device Manager it shows as [Ports (COM&LPT)][Newport ESP301 (COM3)] or similar.
- In both cases you should be able to open a VISA test panel and send a "1ID?\r" query as a test.
- The connection speed is auto-detected on init
- Tested on 2 axes (but should potentially work on 3)

## General use

- If the controller has just been turned on, you should do the auto-calibration of the init sequence; it will move the motors but return them to their original position (plus or minus calibration differences).
- If you've already done a calibration without turning it off, you can say NO; in that case it won't move the motors.

## Notes

- This is rather hackish and ugly code for LabWindows/CVI.
- Not all commands are implemented
- Should work with more than one unit at the same time
- Probably not mutithread safe
- I have another driver for the more recent XPS-RL model, but it's a Linux driver. Find it on GitLab at Dargaud/C_Newport_XPS-RL
